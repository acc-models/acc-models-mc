import glob
from webtools import webtools
import jinja2

tfs_files = sorted(glob.glob('public/**/*.tfs', recursive=True))

for f in tfs_files:
    filename = f[:-3] + 'html'
    webtools.create_optics_plots(f, filename, 'file')
    print('Created ' + filename + '.')

# # for the future
# # generate the navigation structure, which defines the layout of the website (menu bar on the left)
# # jinja is used to fill the nav.yml.template with the information from the imported directory structure
# rdata = {'stages': stages}
# templateLoader = jinja2.FileSystemLoader( searchpath="_scripts/web/templates/" )
# templateEnv = jinja2.Environment(loader=templateLoader )
# tyml = templateEnv.get_template('nav.yml.template')

# webtools.renderfile(['./'], 'nav.yml', tyml, rdata)
