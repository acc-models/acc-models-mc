import os
import glob
import sys
import re        # used to search numbers in strings

import pandas as pnd
import numpy as np
import matplotlib.pyplot as plt 
import matplotlib
from matplotlib.colors import rgb2hex
import datetime
import scipy.constants as constants
import jinja2
import yaml
import collections

# import metaclass
# Python 3 version of the metaclass by the OMC team: pip install tfs-pandas
import tfs

from bokeh.plotting import figure, output_file, output_notebook, show, save, ColumnDataSource
from bokeh.models import Legend, LinearAxis, Range1d, CustomJS, Slider, Span, Panel, Tabs
from bokeh.models.glyphs import Rect
from bokeh.layouts import row, column, gridplot
import bokeh.palettes

plt.rcParams['xtick.direction']='in'
plt.rcParams['ytick.direction']='in'

plt.rcParams['xtick.labelsize']=14
plt.rcParams['ytick.labelsize']=plt.rcParams['xtick.labelsize']
plt.rcParams['axes.titlesize']=plt.rcParams['xtick.labelsize']

plt.rcParams['axes.labelsize']=plt.rcParams['xtick.labelsize']
plt.rcParams['legend.fontsize']=plt.rcParams['xtick.labelsize']
matplotlib.rcParams.update({'font.size': 8*2})
matplotlib.rc('font',**{'family':'serif'})
plt.rcParams["mathtext.fontset"] = "cm"

class webtools:

    def betarel(gamma):
        return np.sqrt(1-1/gamma**2)

    def E0_GeV():
        return constants.physical_constants['proton mass energy equivalent in MeV'][0]/1e3

    def write_tfs(pkl_files):

        for f in pkl_files:
            twiss = pnd.read_pickle(f)
            twiss.drop_duplicates(subset='NAME', keep = 'last', inplace=True)
            tfs.write(f[:-3] + 'tfs', twiss)

    def write_pkl(tfs_files):

        for f in tfs_files:
            twiss = tfs.read(f)
            twiss.drop_duplicates(subset='NAME', keep = 'last', inplace=True)
            twiss.to_pickle(f[:-3] + 'pkl')

    def renderfile(dirnames, name, template, data):
        basedir=''
        
        for dirname in dirnames:
            basedir = os.path.join(basedir,dirname)

        fullname=os.path.join(basedir,name)
        
        with open(fullname,'w') as indexfile:
            indexfile.write(template.render(**data))
        
        print("Successfully created " + fullname)

    def get_optics_at_location(twiss, BI_names):
        optics = {'s': [], 'betx': [], 'bety': [], 'dx': []}
        for name in BI_names:
            optics['s'].append(twiss.S[twiss.NAME == name].values[0])
            optics['betx'].append(twiss.BETA11[twiss.NAME == name].values[0])
            optics['bety'].append(twiss.BETA22[twiss.NAME == name].values[0])
            optics['dx'].append(twiss.DISP1[twiss.NAME == name].values[0])
            
        return optics

    def create_optics_plots(twiss, filename, output, closed_orbit = False):

        # Load TWISS table as dataframe
        if type(twiss) == str:
            if twiss[-3:] == 'tfs':
                twiss = tfs.read(twiss)
            elif twiss[-3:] == 'pkl':
                twiss = pnd.read_pickle(twiss)
        elif type(twiss) == tfs.handler.TfsDataFrame:
            pass
        else: 
            raise TypeError("Twiss input is expected to be a string (either '*.tfs' for a MAD-X TFS table or '*.pkl' for a pickled tfs-pandas TfsDataFrame) or directly a TfsDataFrame.")

        # Extract Twiss Header
        twissHeader = dict(twiss.headers)

        # definition of parameters to be shown when hovering the mouse over the data points
        tooltips = [("parameter", "$name"), ("element", "@name"), ("value [m]", "$y")]
        tooltips_elements = [("element", "@name")]

        if output == 'file':
            # output to static HTML file
            output_file(filename, mode="inline")
        elif output == 'inline':
            output_notebook()

        # define the datasource
        data = pnd.DataFrame(columns = ['s', u"\u03B2x", u"\u03B2y", "Dx"])
        data['s'] = twiss.S
        data['name'] = twiss.NAME
        data['key'] = twiss.KEYWORD
        data['length'] = twiss.L

        if (('TYPE' in twissHeader) and (twissHeader['TYPE'] == 'PTC_TWISS')) or ('NAME' in twissHeader) and (twissHeader['NAME'] == 'PTC_TWISS'):
            # PTC TWISS
            data[u"\u03B2x"] = twiss.BETA11
            data[u"\u03B2y"] = twiss.BETA22
            data['Dx'] = twiss.DISP1
            data['x'] = twiss.X * 1e3
            y_data = False
            try:
                data['y'] = twiss.Y * 1e3
                y_data = True
            except AttributeError:
                data['y'] = twiss.X * 0.
        else:
            # MAD-X TWISS
            data[u"\u03B2x"] = twiss.BETX 
            data[u"\u03B2y"] = twiss.BETY
            data['Dx']       = twiss.DX * webtools.betarel(twissHeader['GAMMA'])
            data['x'] = twiss.X * 1e3
            y_data = False
            try:
                data['y'] = twiss.Y * 1e3
                y_data = True
            except AttributeError:
                data['y'] = twiss.X * 0.        
            try:
                data['Dy']       = twiss.DY * webtools.betarel(twissHeader['GAMMA'])
            except AttributeError:
                data['Dy']       = twiss.DX * 0.0      

        source = ColumnDataSource(data)

        # calculate plot limits based on data range
        # beta-functions:
        bmin = (np.floor(np.min([data[u"\u03B2x"], data[u"\u03B2y"]])/5)) * 5
        bmax = (np.floor(np.max([data[u"\u03B2x"], data[u"\u03B2y"]])/5) + 1) * 5  
        b_p2p = bmax - bmin
        y = Range1d(start = bmin - b_p2p/2, end = bmax)

        # dispersion-function
        dxmin = (np.floor(np.min(data['Dx'])))
        dxmax = (np.floor(np.max(data['Dx'])) + 0.5)  
        dx_p2p = dxmax - dxmin
        dx = Range1d(start = dxmin, end = dxmax + dx_p2p*2)

        # create a new plot with a title and axis labels
        f = figure(title="", x_axis_label='s [m]', y_axis_label=u'\u03B2-functions [m]', width=1000, height=500, x_range=Range1d(0, twiss.LENGTH, bounds="auto"), y_range=y, tools="box_zoom, pan, reset, hover", active_drag = 'box_zoom', tooltips = tooltips)

        f.axis.major_label_text_font = 'times'
        f.axis.axis_label_text_font = 'times'
        f.axis.axis_label_text_font_style = 'normal'
        f.outline_line_color = 'black'
        f.sizing_mode = 'scale_width'

        cols = ['darkblue', 'salmon']

        for j, col in enumerate(data.columns[1:3]):
            f.line('s', col, source=source, name = col, line_width=1.5, line_color = cols[j])

        # Setting the second y axis range name and range
        f.extra_y_ranges = {"disp": dx}

        # define tick locations for each SS or period
        ticks = twiss[['NAME', 'S']]
        ticks = ticks.drop_duplicates(subset='NAME', keep = 'last')
        ticks.set_index(ticks['S'], inplace=True)

        label = ''

        # Setting the range name and range for top x-axis
        f.extra_x_ranges = {"sections": Range1d(start=0.0001, end=twiss.LENGTH, bounds="auto")}

        # Adding the second y axis to the plot.  
        f.add_layout(LinearAxis(y_range_name="disp", axis_label='Dx [m]', axis_label_text_font = 'times', axis_label_text_font_style = 'normal', major_label_text_font = 'times'), 'right')
        dx = f.line('s', 'Dx', source=source, name = 'Dx', line_width=1.5, line_color = 'black', y_range_name="disp")

        legend = Legend(items=[(u"\u03B2x", [f.renderers[0]]), (u"\u03B2y", [f.renderers[1]]), ("Dx", [f.renderers[2]])], location=(10, 165))
        f.add_layout(legend, 'right')
        legend.label_text_font = 'times'
        legend.click_policy = "hide"
        
        tooltips = [("parameter", "$name"), ("element", "@name"), ("value [mm]", "$y")]

        y = (np.floor(min(data['x'])/10) * 10, (np.floor(max(data['x'])/10) + 1) * 10)

        #--------------------------------------------------------
        # add additional axis to plot elements

        f0 = figure(title="", width=1000, height=40, x_range=f.x_range, y_range=(-1.25, 1.25), tools="box_zoom, pan, reset, hover", active_drag = 'box_zoom', tooltips = tooltips_elements)

        f0.axis.visible = False
        f0.grid.visible = False
        f0.outline_line_color = 'white'
        f0.sizing_mode = 'scale_width'

        f0.toolbar.logo = None
        f0.toolbar_location = None

        webtools.plot_lattice_elements(f0, twiss, filename)

        #--------------------------------------------------------
        # add additional axis for orbit plot
        f1 = figure(title="", x_axis_label='s [m]', y_axis_label='x [mm]', width=1000, height=150, x_range=f.x_range, y_range=y, tools="box_zoom, reset", active_drag = 'box_zoom', tooltips = tooltips)

        f1.axis.major_label_text_font = 'times'
        f1.axis.axis_label_text_font = 'times'
        f1.axis.axis_label_text_font_style = 'normal'
        f1.outline_line_color = 'black'
        f1.sizing_mode = 'scale_width'

        if y_data == True:
            f1.line('s', 'x', source=source, name = 'x', line_width=1.5, line_color = 'black')
            f1.line('s', 'y', source=source, name = 'y', line_width=1.5, line_color = 'salmon')
            f1.yaxis.axis_label = 'x, y [mm]' 
            legend = Legend(items=[('x', [f1.renderers[0]]), ('y', [f1.renderers[1]])], location=(10, 20))
            f1.add_layout(legend, 'right')
            legend.label_text_font = 'times'
            legend.click_policy = "hide"
        else:
            f1.line('s', 'x', source=source, name = 'x', line_width=1.5, line_color = 'black')
            f1.yaxis.axis_label = 'x [mm]' 

        f1.toolbar.logo = None
        f1.toolbar_location = None

        if closed_orbit == True:
            if output == 'file':
                save(column([f0, f, f1], sizing_mode = 'scale_width'))
            elif output == 'inline':
                show(column([f0, f, f1], sizing_mode = 'scale_width'))
        else:
            # save the results for standard configurations without orbit plot
            if output == 'file':
                save(column([f0, f], sizing_mode = 'scale_width'))
            elif output == 'inline':
                show(column([f0, f], sizing_mode = 'scale_width'))

    def create_Columndatasource(parameters, values):
        # parameters and data have to be a list

        data = pnd.DataFrame(columns = parameters)
        for i, p in enumerate(parameters):
            data[p] = values[i]

        return ColumnDataSource(data)

    def plot_lattice_elements(figure, twiss, filename):

        # Extract Twiss Header
        twissHeader = dict(twiss.headers)

        pos = twiss.S.values - twiss.L.values/2
        lengths = twiss.L.values
        # modify lengths in order to plot zero-length elements
        lengths[np.where(lengths == 0)[0]] += 0.001

        # BENDS    
        idx = np.array([idx for idx, elem in enumerate(twiss.KEYWORD.values) if 'BEND' in elem])

        idx_0 = idx[twiss.K1L.values[idx] == 0]
        # distinguish F and D half-units of the PS
        idx_1 = idx[twiss.K1L.values[idx] > 0]
        idx_2 = idx[twiss.K1L.values[idx] < 0]

        cols = ['#2ca25f', bokeh.palettes.Reds8[2], bokeh.palettes.Blues8[1]]
        for i, indx in enumerate([idx_0, idx_1, idx_2]):
            source = webtools.create_Columndatasource(['pos', 'width', 'name'], [pos[indx], lengths[indx], np.array(twiss.NAME.values)[indx]])
            figure.rect(x = 'pos', y = 0, width = 'width', height = 2, fill_color=cols[i], line_color = 'black', source = source)

        # QUADRUPOLES
        idx = np.array([idx for idx, elem in enumerate(twiss.KEYWORD.values) if 'QUADRUPOLE' in elem])
        name = np.array(twiss.NAME.values)[idx]
        
        if (twissHeader['SEQUENCE'] == 'PS'): 
            # loc = [map(int, re.findall(r'\d+', n))[-1]%2 for n in name] # extract SS information from string and check whether it is even or odd SS
            loc = [int(re.findall(r'\d+', n)[-1])%2 for n in name]
            idx_1 = idx[np.array(loc) == 1]     # even SS - focusing
            idx_2 = idx[np.array(loc) == 0]     # odd SS - defocusing        
        elif (twissHeader['SEQUENCE'] == 'PSB1') or (twissHeader['SEQUENCE'] == 'LEIR'): 
            idx_1 = idx[np.array([i for i, n in enumerate(name) if 'F' in n])]
            idx_2 = idx[np.array([i for i, n in enumerate(name) if 'D' in n])]

        cols = [bokeh.palettes.Reds8[2], bokeh.palettes.Blues8[1]]
        offset = [0.6, -0.6]
        for i, indx in enumerate([idx_1, idx_2]):
            source = webtools.create_Columndatasource(['pos', 'width', 'name'], [pos[indx], lengths[indx], np.array(twiss.NAME.values)[indx]])
            figure.rect(x = 'pos', y = 0 + offset[i], width = 'width', height = 1.2, fill_color=cols[i], line_color = 'black', source = source)

        # SEXTUPOLES
        try:
            idx = np.array([idx for idx, elem in enumerate(twiss.KEYWORD.values) if 'SEXTUPOLE' in elem])
            name = np.array(twiss.NAME.values)[idx]

            if (twissHeader['SEQUENCE'] == 'PS'):
                # loc = [map(int, re.findall(r'\d+', n))[-1]%2 for n in name] # extract SS information from string and check whether it is even or odd SS
                loc = [int(re.findall(r'\d+', n)[-1])%2 for n in name]
                idx_1 = idx[np.array(loc) == 1]     # even SS - focusing
                idx_2 = idx[np.array(loc) == 0]     # odd SS - defocusing
            elif (twissHeader['SEQUENCE'] == 'LEIR'): 
                idx_1 = idx[np.array([i for i, n in enumerate(name) if 'F' in n])]
                idx_2 = idx[np.array([i for i, n in enumerate(name) if 'D' in n])]

            offset = [0.4, -0.4]
            for i, indx in enumerate([idx_1, idx_2]):
                source = webtools.create_Columndatasource(['pos', 'width', 'name'], [pos[indx], lengths[indx], np.array(twiss.NAME.values)[indx]])
                figure.rect(x = 'pos', y = 0 + offset[i], width = 'width', height = 0.8, fill_color='#fff7bc', line_color = 'black', source = source)

        except IndexError:
            pass
            
        # OCTUPOLES
        try:
            idx = np.array([idx for idx, elem in enumerate(twiss.KEYWORD.values) if 'OCTUPOLE' in elem])
            name = np.array(twiss.NAME.values)[idx]

            if (twissHeader['SEQUENCE'] == 'PS'): 
                # loc = [map(int, re.findall(r'\d+', n))[-1]%2 for n in name] # extract SS information from string and check whether it is even or odd SS
                loc = [int(re.findall(r'\d+', n)[-1])%2 for n in name]
                idx_1 = idx[np.array(loc) == 1]     # even SS - focusing
                idx_2 = idx[np.array(loc) == 0]     # odd SS - defocusing        

                offset = [0.3, -0.3]
                for i, indx in enumerate([idx_1, idx_2]):
                    source = webtools.create_Columndatasource(['pos', 'width', 'name'], [pos[indx], lengths[indx], np.array(twiss.NAME.values)[indx]])
                    figure.rect(x = 'pos', y = 0 + offset[i], width = 'width', height = 0.6, fill_color='#756bb1', line_color = 'black', source = source)

        except IndexError:
            pass

        # KICKERS
        try:
            idx = np.array([idx for idx, elem in enumerate(twiss.KEYWORD.values) if 'KICKER' in elem])
            source = webtools.create_Columndatasource(['pos', 'width', 'name'], [pos[idx], lengths[idx], np.array(twiss.NAME.values)[idx]])
            figure.rect(x = 'pos', y = 0, width = 'width', height = 2, fill_color='#1b9e77', line_color = 'black', source = source)
        except IndexError:
            pass
        
        # CAVITIES
        try:
            idx = np.array([idx for idx, elem in enumerate(twiss.KEYWORD.values) if 'CAVITY' in elem])
            source = webtools.create_Columndatasource(['pos', 'width', 'name'], [pos[idx], lengths[idx], np.array(twiss.NAME.values)[idx]])
            figure.rect(x = 'pos', y =  0.7, width = 'width', height = 1, fill_color='#e7e1ef', line_color = 'black', source = source)
            figure.rect(x = 'pos', y = -0.7, width = 'width', height = 1, fill_color='#e7e1ef', line_color = 'black', source = source)
        except IndexError:
            pass

        # MONITORS and INSTRUMENTS
        try:
            idx = np.array([idx for idx, elem in enumerate(twiss.KEYWORD.values) if ('MONITOR' in elem) or ('INSTRUMENT' in elem)])
            source = webtools.create_Columndatasource(['pos', 'width', 'name'], [pos[idx], lengths[idx], np.array(twiss.NAME.values)[idx]])
            figure.rect(x = 'pos', y = 0, width = 'width', height = 2, fill_color='gray', line_color = 'black', source = source)
        except IndexError:
            pass
        
        # SOLENOIDS
        try:
            idx = np.array([idx for idx, elem in enumerate(twiss.KEYWORD.values) if 'SOLENOID' in elem])
            source = webtools.create_Columndatasource(['pos', 'width', 'name'], [pos[idx], lengths[idx], np.array(twiss.NAME.values)[idx]])
            figure.rect(x = 'pos', y = 0, width = 'width', height = 0.5, fill_color='#a4a4a4', line_color = 'black', source = source)
        except IndexError:
            pass

        # horizontal line at zero
        source = webtools.create_Columndatasource(['pos', 'name'], [[0, twiss.S.iloc[-1]], ['START', 'END']])
        figure.line('pos', 0., line_width=.5, line_color = 'black', source = source)  


