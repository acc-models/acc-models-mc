---
template: overrides/main.html
---

<h1> Muon collider optics for 3.0 TeV</h1>

## Twiss functions
The Twiss functions of this configuration are shown in the interactive plot below. You can zoom in or hover over any curve to obtain more information about the function's value at a specific element. 

<object width="100%" height="810" data="mc3tev_v1.2.html"></object> 